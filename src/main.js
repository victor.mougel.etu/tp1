let html=' ';

const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];


//la propriété base est 'tomate'
const baseTomate=data.filter(({base})=>base=="tomate");

//le prix petit format est inférieur à 6€
const prixSmallInferieur6=data.filter(({price_small})=>price_small < 6);

const regex=/([i]).*?\1/;
//le nom contient deux fois la lettre "i"
const deuxFoisLettreI=data.filter(({name})=>regex.test(name));

// //Methode foreEach
// deuxFoisLettreI.forEach(({image,price_small,name,price_large}) =>html+=`<article class="pizzaThumbnail">
//                             <a href="${image}"> 
//                                 <img src="${image}"/>
//                                 <section>
//                                     <h4>${name}</h4>
//                                     <ul>
//                                         <li>Prix petit format : ${(price_small.toFixed(2))} €</li>
//                                         <li>Prix grand format : ${price_large.toFixed(2)} €</li>
//                                     </ul>
//                                 </section>
//                             </a>
//                             </article>`);



const reducer = (html, {image, name, price_small, price_large}) => html + `<article class="pizzaThumbnail">
                    <a href="${image}">
                    <img src="${image}"/>
                    <section>
                        <h4>${name}</h4>
                        <ul>
                            <li>Prix petit fromat : ${price_small.toPrecision(2)} €</li>
                            <li>Prix grand fromat : ${price_large.toPrecision(2)} €</li>
                        </ul>
                    </section>
                    </a>
                    </article>`;


document.querySelector('.pageContent').innerHTML = data.reduce(reducer, '');
