"use strict";

var html = ' ';
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; //la propriété base est 'tomate'

var baseTomate = data.filter(function (_ref) {
  var base = _ref.base;
  return base == "tomate";
}); //le prix petit format est inférieur à 6€

var prixSmallInferieur6 = data.filter(function (_ref2) {
  var price_small = _ref2.price_small;
  return price_small < 6;
});
var regex = /([i]).*?\1/; //le nom contient deux fois la lettre "i"

var deuxFoisLettreI = data.filter(function (_ref3) {
  var name = _ref3.name;
  return regex.test(name);
}); // //Methode foreEach
// deuxFoisLettreI.forEach(({image,price_small,name,price_large}) =>html+=`<article class="pizzaThumbnail">
//                             <a href="${image}"> 
//                                 <img src="${image}"/>
//                                 <section>
//                                     <h4>${name}</h4>
//                                     <ul>
//                                         <li>Prix petit format : ${(price_small.toFixed(2))} €</li>
//                                         <li>Prix grand format : ${price_large.toFixed(2)} €</li>
//                                     </ul>
//                                 </section>
//                             </a>
//                             </article>`);

var reducer = function reducer(html, _ref4) {
  var image = _ref4.image,
      name = _ref4.name,
      price_small = _ref4.price_small,
      price_large = _ref4.price_large;
  return html + "<article class=\"pizzaThumbnail\">\n                    <a href=\"".concat(image, "\">\n                    <img src=\"").concat(image, "\"/>\n                    <section>\n                        <h4>").concat(name, "</h4>\n                        <ul>\n                            <li>Prix petit fromat : ").concat(price_small.toPrecision(2), " \u20AC</li>\n                            <li>Prix grand fromat : ").concat(price_large.toPrecision(2), " \u20AC</li>\n                        </ul>\n                    </section>\n                    </a>\n                    </article>");
};

document.querySelector('.pageContent').innerHTML = data.reduce(reducer, '');
//# sourceMappingURL=main.js.map